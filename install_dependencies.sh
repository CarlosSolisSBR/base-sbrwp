#!/bin/bash

echo "this scripts installs: yarn, VirtualBox and Ansible"
echo "Installing "

# Install yarn
echo "Installing yarn"
npm install --global yarn

# Install VirtualBox
echo "Installing VirtualBox"
sudo apt-get install virtualbox

# Install Ansible
echo "Installing Ansible"
pip install --user ansible==2.7.5

echo "To install Vagrant visit https://roots.io/getting-started/docs/ubuntu-linux-development-environment-trellis/"