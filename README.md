# base-sbrwp

This is a base to start a new Wordpress project.

For further documentation:
https://roots.io/
https://discourse.roots.io/
https://github.com/roots/roots-example-project.com

Dependencies:
- Composer (Optional)
- python-pip
- Node >= 8.0.0
- yarn
- VirtualBox >= 4.3.10
- Vagrant >= 1.8.5
- Ansible >= 2.5.3-2.7.5